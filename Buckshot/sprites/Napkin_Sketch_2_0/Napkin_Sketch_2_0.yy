{
    "id": "07b531fe-9347-4aca-ad6a-ac1acfca245e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Napkin_Sketch_2_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3299,
    "bbox_left": 0,
    "bbox_right": 4499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f48fb93-2c22-43ea-8e06-8f56bfaeab32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b531fe-9347-4aca-ad6a-ac1acfca245e",
            "compositeImage": {
                "id": "5964df5a-963c-40d9-8db6-97d2250b5a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f48fb93-2c22-43ea-8e06-8f56bfaeab32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c66ae0-41a2-4ef6-9bcf-caf2b37848b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f48fb93-2c22-43ea-8e06-8f56bfaeab32",
                    "LayerId": "a0c1457f-9acd-4f0b-b8a0-401a6f9937ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3300,
    "layers": [
        {
            "id": "a0c1457f-9acd-4f0b-b8a0-401a6f9937ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07b531fe-9347-4aca-ad6a-ac1acfca245e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4500,
    "xorig": 0,
    "yorig": 0
}