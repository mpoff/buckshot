{
    "id": "525247e9-ea4b-4b2d-b75f-488fe5ecc9a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ButtonProto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 58,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "477c6b01-2ec9-4727-b15a-6b6f2fbcfebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "525247e9-ea4b-4b2d-b75f-488fe5ecc9a2",
            "compositeImage": {
                "id": "a6e408c1-a8d6-4b7e-b186-9acb56cf35aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "477c6b01-2ec9-4727-b15a-6b6f2fbcfebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5e0a23-4d9c-4266-ae99-afb347e64f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "477c6b01-2ec9-4727-b15a-6b6f2fbcfebb",
                    "LayerId": "7e552046-18e8-4c4f-b2dd-6dd3aa1d400c"
                }
            ]
        },
        {
            "id": "4bef95ee-f101-47cc-816f-832de8167168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "525247e9-ea4b-4b2d-b75f-488fe5ecc9a2",
            "compositeImage": {
                "id": "0d39f2a6-c982-4908-8f37-f57af4c83ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bef95ee-f101-47cc-816f-832de8167168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee8a5ca-b53c-42fc-98ed-77db0105a1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bef95ee-f101-47cc-816f-832de8167168",
                    "LayerId": "7e552046-18e8-4c4f-b2dd-6dd3aa1d400c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7e552046-18e8-4c4f-b2dd-6dd3aa1d400c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "525247e9-ea4b-4b2d-b75f-488fe5ecc9a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 31
}