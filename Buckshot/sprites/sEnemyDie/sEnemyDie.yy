{
    "id": "65265b6d-19fe-4b42-a3b6-dc6e452ed7cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55d22bd3-1378-457b-9407-bbe5669da870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65265b6d-19fe-4b42-a3b6-dc6e452ed7cd",
            "compositeImage": {
                "id": "1342232b-6873-4e2d-9052-2955b36ef5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55d22bd3-1378-457b-9407-bbe5669da870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "753c3286-70b9-47b3-bd7b-bc0c8b52cae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55d22bd3-1378-457b-9407-bbe5669da870",
                    "LayerId": "39b3d874-aaaa-4d87-af96-b369337d2331"
                }
            ]
        },
        {
            "id": "8948a3bc-801f-4220-aefd-332b9f66280f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65265b6d-19fe-4b42-a3b6-dc6e452ed7cd",
            "compositeImage": {
                "id": "8c694443-1f86-49a8-b8b9-9369dc717a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8948a3bc-801f-4220-aefd-332b9f66280f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7abf6c9a-ea8b-4f89-9f67-ba056ce0c995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8948a3bc-801f-4220-aefd-332b9f66280f",
                    "LayerId": "39b3d874-aaaa-4d87-af96-b369337d2331"
                }
            ]
        },
        {
            "id": "94d29856-71c6-495a-b6fc-00821e125202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65265b6d-19fe-4b42-a3b6-dc6e452ed7cd",
            "compositeImage": {
                "id": "68c262c6-af1a-4245-834e-78b1806ce28a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d29856-71c6-495a-b6fc-00821e125202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08c656c-7f9e-445d-9dbb-3e7a22404aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d29856-71c6-495a-b6fc-00821e125202",
                    "LayerId": "39b3d874-aaaa-4d87-af96-b369337d2331"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "39b3d874-aaaa-4d87-af96-b369337d2331",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65265b6d-19fe-4b42-a3b6-dc6e452ed7cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}