{
    "id": "f8546c32-5995-413c-bfa9-0fcb1020cdce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlope30gL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4766d2b4-cd1a-4341-8191-7d6a4c7b3520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8546c32-5995-413c-bfa9-0fcb1020cdce",
            "compositeImage": {
                "id": "604202f5-e7f5-4338-9a4d-f143621c8a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4766d2b4-cd1a-4341-8191-7d6a4c7b3520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d8232d-edf8-4e81-b3ef-8e77030b0246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4766d2b4-cd1a-4341-8191-7d6a4c7b3520",
                    "LayerId": "3fe59942-df09-4ada-95f6-45b9566f2239"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3fe59942-df09-4ada-95f6-45b9566f2239",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8546c32-5995-413c-bfa9-0fcb1020cdce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}