{
    "id": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98c37b9b-9c23-4202-8dd4-3152b82883a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "c96df969-fee9-48c7-a1ae-592bf53d41ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c37b9b-9c23-4202-8dd4-3152b82883a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e09ba5-e3b1-4ae2-a484-82f958c6ae50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c37b9b-9c23-4202-8dd4-3152b82883a4",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "cc59bfd5-8768-4508-b770-5e4111ae0def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "e064c8aa-9cb0-4593-9838-f58713548f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc59bfd5-8768-4508-b770-5e4111ae0def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad0b246-d5b4-414d-abdf-e3ef8379e058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc59bfd5-8768-4508-b770-5e4111ae0def",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "fab5c4f0-1da0-48b5-a46d-8115727a00a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "bfe6cdf1-1252-43d1-b4bf-46589adb28cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab5c4f0-1da0-48b5-a46d-8115727a00a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54561072-431c-4405-b9a0-89b8b72fe675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab5c4f0-1da0-48b5-a46d-8115727a00a8",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "00be8218-bd62-4647-b9a8-1eb2daecc297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "4873b212-c34f-49d0-8426-a362fa218dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00be8218-bd62-4647-b9a8-1eb2daecc297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683e7e2b-767e-4be9-8e46-b018f925e800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00be8218-bd62-4647-b9a8-1eb2daecc297",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "95f1dacf-a32c-4569-9f11-593aa2dacd7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "02fe9dbb-1a46-4115-964e-13eb8eca0ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95f1dacf-a32c-4569-9f11-593aa2dacd7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "964b9c8f-2046-449c-8f46-d57bd4f03b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f1dacf-a32c-4569-9f11-593aa2dacd7b",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "8ef83c99-22a2-45fa-8630-8067e8115fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "3db1148c-90c9-4bc6-98f3-d03debcee470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef83c99-22a2-45fa-8630-8067e8115fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1f57b9-e62d-4549-8489-790d18d975e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef83c99-22a2-45fa-8630-8067e8115fe0",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "a526dfa8-3d06-475f-b8f7-5fa07b74b7e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "7822cada-368f-4b92-b42c-961bc9f2d859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a526dfa8-3d06-475f-b8f7-5fa07b74b7e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9c5620-f75c-44f5-8b71-91e254eb9337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a526dfa8-3d06-475f-b8f7-5fa07b74b7e9",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        },
        {
            "id": "9642eeef-e277-4c19-a83f-b8ece570eda3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "compositeImage": {
                "id": "edd628e0-29e2-46d3-aa29-340d81153291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9642eeef-e277-4c19-a83f-b8ece570eda3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8656273-8f77-49ff-8c6a-0ccbb38b2ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9642eeef-e277-4c19-a83f-b8ece570eda3",
                    "LayerId": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "d3c4c728-da07-4cbf-aba3-aee00ec9d9c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}