{
    "id": "81593ccd-9305-4531-818c-7dc281a56be4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerSlide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "561b37f7-9dc9-4078-86df-13c0fb2af2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "compositeImage": {
                "id": "43ea4180-7b28-480f-abb6-f65542de41e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561b37f7-9dc9-4078-86df-13c0fb2af2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc715c9-cf3d-4d0f-9f89-9ab5f7d0604f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561b37f7-9dc9-4078-86df-13c0fb2af2b1",
                    "LayerId": "b8f35f19-1634-4bfe-9951-66319b958c28"
                }
            ]
        },
        {
            "id": "a6deb23f-71ff-4106-8811-7824ae453ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "compositeImage": {
                "id": "31847afc-0e5b-4fa4-8b06-6f3052b2796b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6deb23f-71ff-4106-8811-7824ae453ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee5e42ed-52b3-4c2a-8c9c-95965b8e35b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6deb23f-71ff-4106-8811-7824ae453ca7",
                    "LayerId": "b8f35f19-1634-4bfe-9951-66319b958c28"
                }
            ]
        },
        {
            "id": "59789d8f-514b-47b4-8b76-af7f6b77f2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "compositeImage": {
                "id": "79cc8faa-41a7-4d1c-be49-53bb1fbcec31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59789d8f-514b-47b4-8b76-af7f6b77f2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f29a557-b3e2-4377-867f-dde03a6dfe55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59789d8f-514b-47b4-8b76-af7f6b77f2b8",
                    "LayerId": "b8f35f19-1634-4bfe-9951-66319b958c28"
                }
            ]
        },
        {
            "id": "6fac4604-00ef-44b8-aa59-90341224fc1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "compositeImage": {
                "id": "aedc84ef-4f24-403c-b95b-7adb334df37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fac4604-00ef-44b8-aa59-90341224fc1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d72cc0f-36e2-4bc2-9aed-dec0877c6753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fac4604-00ef-44b8-aa59-90341224fc1c",
                    "LayerId": "b8f35f19-1634-4bfe-9951-66319b958c28"
                }
            ]
        },
        {
            "id": "56f07cea-dc09-46db-8c36-0ca4e3f78df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "compositeImage": {
                "id": "287848c5-10b9-453a-89cd-6061203b28ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f07cea-dc09-46db-8c36-0ca4e3f78df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af5f3be1-1eec-4df1-b5a7-b058843995f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f07cea-dc09-46db-8c36-0ca4e3f78df4",
                    "LayerId": "b8f35f19-1634-4bfe-9951-66319b958c28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b8f35f19-1634-4bfe-9951-66319b958c28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81593ccd-9305-4531-818c-7dc281a56be4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 16
}