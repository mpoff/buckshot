{
    "id": "717d38c1-a9e9-4754-b945-90e2477641b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy2Shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "895fbd5e-f43d-4974-8606-3cf742cfc6a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "717d38c1-a9e9-4754-b945-90e2477641b1",
            "compositeImage": {
                "id": "8dcb235e-1309-480c-8f63-47e360b51c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895fbd5e-f43d-4974-8606-3cf742cfc6a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a70a31-38e6-42bd-b45c-1967e4fb29ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895fbd5e-f43d-4974-8606-3cf742cfc6a9",
                    "LayerId": "5830ff5d-888a-4737-bb54-3542d6066545"
                }
            ]
        },
        {
            "id": "019861e7-9152-4889-aa7e-f1851f7e3d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "717d38c1-a9e9-4754-b945-90e2477641b1",
            "compositeImage": {
                "id": "2e2b489c-9ff8-4f6a-b332-4a8668107822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019861e7-9152-4889-aa7e-f1851f7e3d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c52ac0-7540-48d5-af18-b743109910f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019861e7-9152-4889-aa7e-f1851f7e3d5e",
                    "LayerId": "5830ff5d-888a-4737-bb54-3542d6066545"
                }
            ]
        },
        {
            "id": "86da7948-5c6a-42f8-81f6-693446b38ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "717d38c1-a9e9-4754-b945-90e2477641b1",
            "compositeImage": {
                "id": "ab11190b-b690-4bed-978d-2ab22be018aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86da7948-5c6a-42f8-81f6-693446b38ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab5432e-8555-4fc6-9807-65ed8ba75ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86da7948-5c6a-42f8-81f6-693446b38ae4",
                    "LayerId": "5830ff5d-888a-4737-bb54-3542d6066545"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5830ff5d-888a-4737-bb54-3542d6066545",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "717d38c1-a9e9-4754-b945-90e2477641b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 16
}