{
    "id": "afc15231-a9f7-4d56-999e-066ae3f84176",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_angryboi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9d101d5-da86-4026-b327-d9f7710f1c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afc15231-a9f7-4d56-999e-066ae3f84176",
            "compositeImage": {
                "id": "b170b2e4-370e-4a5b-a60d-b1318d53ce36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d101d5-da86-4026-b327-d9f7710f1c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d1be7c-c346-4282-ab3f-a06956297a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d101d5-da86-4026-b327-d9f7710f1c01",
                    "LayerId": "3687d90f-a3b2-4d6c-9e10-fed3226204a7"
                },
                {
                    "id": "44ede6ed-6447-497e-8e40-83bc6e75b2bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d101d5-da86-4026-b327-d9f7710f1c01",
                    "LayerId": "aff37ed1-908b-40d8-b18a-2c0c4a546f93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3687d90f-a3b2-4d6c-9e10-fed3226204a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afc15231-a9f7-4d56-999e-066ae3f84176",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "aff37ed1-908b-40d8-b18a-2c0c4a546f93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afc15231-a9f7-4d56-999e-066ae3f84176",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}