{
    "id": "6c92890a-486a-4038-8461-ea23b8383436",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlopeL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "123abe48-f679-414f-a261-022bb2a2646b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c92890a-486a-4038-8461-ea23b8383436",
            "compositeImage": {
                "id": "97d388b1-5d9c-4ded-8730-91b7172f216c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123abe48-f679-414f-a261-022bb2a2646b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f5d07f-8dca-4492-accd-24592e67721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123abe48-f679-414f-a261-022bb2a2646b",
                    "LayerId": "2a6b4d62-f1b6-4b62-8d18-fe5e469ebdd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2a6b4d62-f1b6-4b62-8d18-fe5e469ebdd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c92890a-486a-4038-8461-ea23b8383436",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}