{
    "id": "3d21042d-3a84-40cc-93ce-72819f775dd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12d14350-077b-4445-bd9c-96a0c2945355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d21042d-3a84-40cc-93ce-72819f775dd4",
            "compositeImage": {
                "id": "16291723-30a4-4b84-8a94-a22ed67b83a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d14350-077b-4445-bd9c-96a0c2945355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b925be87-4669-4a48-bfc1-ffd93a9a66ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d14350-077b-4445-bd9c-96a0c2945355",
                    "LayerId": "458f2ce3-46cc-4863-9645-7a72582e8644"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "458f2ce3-46cc-4863-9645-7a72582e8644",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d21042d-3a84-40cc-93ce-72819f775dd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 4
}