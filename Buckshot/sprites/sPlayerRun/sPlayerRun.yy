{
    "id": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b30a03f6-158f-4122-aac5-cae8984549fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "compositeImage": {
                "id": "0e3873b7-fd1b-4ca3-a16c-f6abc24e55e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30a03f6-158f-4122-aac5-cae8984549fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22affad3-27b4-4982-bf1b-5642a63d990d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30a03f6-158f-4122-aac5-cae8984549fb",
                    "LayerId": "168226be-cb09-40ae-8bd8-9ef3e8c0993a"
                }
            ]
        },
        {
            "id": "29c5b389-f45d-44a3-a9a4-803aa2ca38a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "compositeImage": {
                "id": "bab9b59d-c211-4897-8ad7-128f600e3753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c5b389-f45d-44a3-a9a4-803aa2ca38a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78866b4-2af2-48b2-9c85-2e358b220abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c5b389-f45d-44a3-a9a4-803aa2ca38a5",
                    "LayerId": "168226be-cb09-40ae-8bd8-9ef3e8c0993a"
                }
            ]
        },
        {
            "id": "4a6451a5-b1b7-41fe-a242-d14787cb04b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "compositeImage": {
                "id": "0f6060c6-305f-4785-91c7-a5a2e22bd7eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6451a5-b1b7-41fe-a242-d14787cb04b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55fc187-f832-48e9-b039-4d41b4cf27e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6451a5-b1b7-41fe-a242-d14787cb04b2",
                    "LayerId": "168226be-cb09-40ae-8bd8-9ef3e8c0993a"
                }
            ]
        },
        {
            "id": "223a3170-cf98-43a4-9543-5691f73b3573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "compositeImage": {
                "id": "47fa499b-e8ac-4393-b985-5f970b143cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223a3170-cf98-43a4-9543-5691f73b3573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44c134b-5833-44e1-878c-65750fa27b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223a3170-cf98-43a4-9543-5691f73b3573",
                    "LayerId": "168226be-cb09-40ae-8bd8-9ef3e8c0993a"
                }
            ]
        },
        {
            "id": "4858d4b2-9659-4e5c-b1f9-f730a2d570ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "compositeImage": {
                "id": "6fba9b2d-b7c0-437d-a680-579789c16623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4858d4b2-9659-4e5c-b1f9-f730a2d570ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c27eaff-c325-496b-afb5-66ebf93523c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4858d4b2-9659-4e5c-b1f9-f730a2d570ac",
                    "LayerId": "168226be-cb09-40ae-8bd8-9ef3e8c0993a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "168226be-cb09-40ae-8bd8-9ef3e8c0993a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a7f2499-f877-4e3d-9163-78fa6ac17d02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 16
}