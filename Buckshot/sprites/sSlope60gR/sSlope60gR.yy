{
    "id": "04b3d551-d9af-4f94-9848-69a3b61895f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlope60gR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f504d6e-a4c2-4247-82bc-61c438aa99f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04b3d551-d9af-4f94-9848-69a3b61895f0",
            "compositeImage": {
                "id": "0f612211-904a-4427-9bd1-4856cc94ce27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f504d6e-a4c2-4247-82bc-61c438aa99f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41580f73-b139-4f17-83ee-e3e7120c6570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f504d6e-a4c2-4247-82bc-61c438aa99f3",
                    "LayerId": "c94de710-5b86-48b1-8ba9-56d56e7896f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c94de710-5b86-48b1-8ba9-56d56e7896f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04b3d551-d9af-4f94-9848-69a3b61895f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}