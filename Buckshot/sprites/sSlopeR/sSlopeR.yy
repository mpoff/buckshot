{
    "id": "d950dfc5-606d-4401-b8e5-1e279289dbc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlopeR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c5ccf8c-9b5a-4218-b0df-343dac8abe0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d950dfc5-606d-4401-b8e5-1e279289dbc0",
            "compositeImage": {
                "id": "1222369c-d9d9-408a-963c-6319f316c569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5ccf8c-9b5a-4218-b0df-343dac8abe0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35822980-216e-43b5-8cac-60149ce0a9fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5ccf8c-9b5a-4218-b0df-343dac8abe0e",
                    "LayerId": "b2e2b8aa-34ad-4b22-9562-9aa8ad82c915"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2e2b8aa-34ad-4b22-9562-9aa8ad82c915",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d950dfc5-606d-4401-b8e5-1e279289dbc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}