{
    "id": "f1190ee5-cfd8-4236-be7a-2aaf064ba151",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9d497d1-f3f7-428b-9f81-830be8044348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1190ee5-cfd8-4236-be7a-2aaf064ba151",
            "compositeImage": {
                "id": "39e8740d-feee-4a38-b84f-8019f3bfc9cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d497d1-f3f7-428b-9f81-830be8044348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9628ab19-40d9-4cbe-a453-215fc20272a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d497d1-f3f7-428b-9f81-830be8044348",
                    "LayerId": "6ef9ecb7-a4f7-4659-b02f-8d33a5a7f90c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "6ef9ecb7-a4f7-4659-b02f-8d33a5a7f90c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1190ee5-cfd8-4236-be7a-2aaf064ba151",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 6
}