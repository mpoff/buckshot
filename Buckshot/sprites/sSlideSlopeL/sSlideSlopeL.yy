{
    "id": "3a934920-93eb-4044-be9a-0901e6ab7793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlideSlopeL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "790cd50d-eb7e-4efc-9153-04f61582c731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a934920-93eb-4044-be9a-0901e6ab7793",
            "compositeImage": {
                "id": "95051952-36c1-415a-9036-c8ceb525b010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "790cd50d-eb7e-4efc-9153-04f61582c731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b53b887-928f-4b23-9585-04983b27cd0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790cd50d-eb7e-4efc-9153-04f61582c731",
                    "LayerId": "e789d6f7-4b78-4f5d-b12e-4220e39c7a67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e789d6f7-4b78-4f5d-b12e-4220e39c7a67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a934920-93eb-4044-be9a-0901e6ab7793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}