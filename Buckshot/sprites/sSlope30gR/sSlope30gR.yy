{
    "id": "4e620b08-d44f-4d3b-a0c4-e7d019667532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlope30gR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d20463c-48e3-49cb-9a49-70321b07b84e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e620b08-d44f-4d3b-a0c4-e7d019667532",
            "compositeImage": {
                "id": "83531453-ef28-42d7-bcf9-db614cca74b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d20463c-48e3-49cb-9a49-70321b07b84e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3432b6d-76ad-40df-9a8d-8e3cc148e23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d20463c-48e3-49cb-9a49-70321b07b84e",
                    "LayerId": "c034cdaa-0a33-40a8-8d83-ef6816a5f54e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c034cdaa-0a33-40a8-8d83-ef6816a5f54e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e620b08-d44f-4d3b-a0c4-e7d019667532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}