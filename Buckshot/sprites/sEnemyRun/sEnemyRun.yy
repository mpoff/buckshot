{
    "id": "26f582b9-8428-48c9-ba44-38ef5b04982f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c41cda14-d68a-4ae2-9cc3-dc4797350dd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "c41f9bbb-af23-4e56-b4ff-86556ac21441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41cda14-d68a-4ae2-9cc3-dc4797350dd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc53d8f-b3b6-4632-b730-b8b9153d1a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41cda14-d68a-4ae2-9cc3-dc4797350dd2",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        },
        {
            "id": "913da126-04ea-458a-9681-272119c242fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "589c9f7c-e1f4-41da-92e1-645d96422f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913da126-04ea-458a-9681-272119c242fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bbb172e-e73b-44fd-a58d-192eb2428be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913da126-04ea-458a-9681-272119c242fb",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        },
        {
            "id": "5e6e68c0-7c12-4ed1-81ee-6e9dc8cb60b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "0a7160c9-3981-49e5-945d-e8b9e25bbfb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e6e68c0-7c12-4ed1-81ee-6e9dc8cb60b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc962866-47f6-4676-9f6c-fbcb9566a11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e6e68c0-7c12-4ed1-81ee-6e9dc8cb60b4",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        },
        {
            "id": "71794ef9-590f-469f-94c9-ac1c97d9bbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "d9fc815d-cc67-4c5e-bc88-c2746f96d482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71794ef9-590f-469f-94c9-ac1c97d9bbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a20e33-3a08-4989-a302-5024e29162ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71794ef9-590f-469f-94c9-ac1c97d9bbbb",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        },
        {
            "id": "abd47625-754d-46ef-b41d-0b21fc22b506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "36d60af9-a0cc-40fe-bcee-912dec681876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd47625-754d-46ef-b41d-0b21fc22b506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a08332-d659-4b1e-bc9b-707bc3e633cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd47625-754d-46ef-b41d-0b21fc22b506",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        },
        {
            "id": "e7cea4b2-38ea-46f9-b6c8-15adc59f2a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "compositeImage": {
                "id": "0b9708b4-2302-4afd-bb29-bbb4618c659f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7cea4b2-38ea-46f9-b6c8-15adc59f2a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2980ba-b439-4d33-93a1-55104eb9edf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7cea4b2-38ea-46f9-b6c8-15adc59f2a2e",
                    "LayerId": "452fe477-a70b-4619-ba8f-bb03a2cf74d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "452fe477-a70b-4619-ba8f-bb03a2cf74d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26f582b9-8428-48c9-ba44-38ef5b04982f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 16
}