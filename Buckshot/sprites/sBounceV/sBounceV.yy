{
    "id": "917b8ac4-0b2b-4860-a37e-83aee8b8dc38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBounceV",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f79ff3e3-c539-4a6c-9aa2-9e573b50ee20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "917b8ac4-0b2b-4860-a37e-83aee8b8dc38",
            "compositeImage": {
                "id": "59fcfe1b-f53b-411a-b5f9-c7acff36a771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f79ff3e3-c539-4a6c-9aa2-9e573b50ee20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29e228b-3dc6-4a93-b47f-869ed15eb5f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f79ff3e3-c539-4a6c-9aa2-9e573b50ee20",
                    "LayerId": "44eff89c-2941-4706-b004-aebb2419e2fa"
                }
            ]
        },
        {
            "id": "1607db0d-49a7-4270-8d0b-b51d9811db0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "917b8ac4-0b2b-4860-a37e-83aee8b8dc38",
            "compositeImage": {
                "id": "710b5e11-80f6-4770-bfa4-29ccf35740aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1607db0d-49a7-4270-8d0b-b51d9811db0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ebd7ec-3e38-454b-b538-b270e292aff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1607db0d-49a7-4270-8d0b-b51d9811db0d",
                    "LayerId": "44eff89c-2941-4706-b004-aebb2419e2fa"
                }
            ]
        },
        {
            "id": "502f68e3-4942-4ac9-a8c1-26083deea18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "917b8ac4-0b2b-4860-a37e-83aee8b8dc38",
            "compositeImage": {
                "id": "eee9bdf2-ad8d-4715-88c6-c11d0db55981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "502f68e3-4942-4ac9-a8c1-26083deea18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c65824-5289-411d-addb-7b00b56e564a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "502f68e3-4942-4ac9-a8c1-26083deea18a",
                    "LayerId": "44eff89c-2941-4706-b004-aebb2419e2fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "44eff89c-2941-4706-b004-aebb2419e2fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "917b8ac4-0b2b-4860-a37e-83aee8b8dc38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 34
}