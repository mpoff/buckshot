{
    "id": "fc056e48-6e43-4f12-b776-558261340949",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c42792b-5d36-49ee-ae18-b21ca4a8d4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "51d18d2f-3e12-4ec4-8201-3d446848fe79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c42792b-5d36-49ee-ae18-b21ca4a8d4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66d09ff-7ca6-4a9f-ad8a-d6a7555894fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c42792b-5d36-49ee-ae18-b21ca4a8d4a5",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "65282344-ddcb-4bdd-aa22-05dca8420200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "bed0e05e-69db-4349-a49e-6551a834b387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65282344-ddcb-4bdd-aa22-05dca8420200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e8ea25-457e-43b8-98b7-0c36bae29620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65282344-ddcb-4bdd-aa22-05dca8420200",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "2a9622fc-3cfa-4038-bb84-eedc40cffbda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "2ee14ef0-11dd-47f1-89a8-6c733daf055a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9622fc-3cfa-4038-bb84-eedc40cffbda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db944465-38bc-48b5-bd48-92d96d741cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9622fc-3cfa-4038-bb84-eedc40cffbda",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "a1448274-6f5a-46a6-9a52-74e33797e926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "93dffe11-b6d8-4e95-8d1c-ade75c29bf81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1448274-6f5a-46a6-9a52-74e33797e926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e1b501-11e0-447c-9d75-9df8ebd14985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1448274-6f5a-46a6-9a52-74e33797e926",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "65961393-495b-4e75-8444-0b437db8cff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "03f33106-4358-4f54-a2b1-9bace34a2691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65961393-495b-4e75-8444-0b437db8cff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "707d6e41-683a-4184-9484-4b3a9283dd78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65961393-495b-4e75-8444-0b437db8cff1",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "266ff06b-af6a-4530-9854-f10c62f716bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "54000a22-3e0e-4968-baf9-394896cdea52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266ff06b-af6a-4530-9854-f10c62f716bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4975d79b-792a-43ea-8ee4-72ba24927131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266ff06b-af6a-4530-9854-f10c62f716bf",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "d81130f8-592f-4cb6-8831-290aaaa59c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "47270d1a-ea92-4224-8943-687e2c2afd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d81130f8-592f-4cb6-8831-290aaaa59c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51dee27f-a1a1-4b34-a9e5-9d4adfdc0585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d81130f8-592f-4cb6-8831-290aaaa59c5f",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        },
        {
            "id": "7163254f-078c-42db-8ae2-c9ea525843ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "compositeImage": {
                "id": "aa0afe51-7f59-48e4-9374-3f1a0afede40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7163254f-078c-42db-8ae2-c9ea525843ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95310bc1-d4e6-4a6d-abdf-cf4d6d87c2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7163254f-078c-42db-8ae2-c9ea525843ac",
                    "LayerId": "6c38afeb-5593-4c20-83ce-bf37f8c70efd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6c38afeb-5593-4c20-83ce-bf37f8c70efd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc056e48-6e43-4f12-b776-558261340949",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}