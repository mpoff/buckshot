{
    "id": "8b55a47d-8551-423e-b66b-459e3f34b41a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3fe709f-74fd-4a92-a51e-eae8affc82ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b55a47d-8551-423e-b66b-459e3f34b41a",
            "compositeImage": {
                "id": "aa8e331c-bcfc-4a38-af36-e6cbca0275cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3fe709f-74fd-4a92-a51e-eae8affc82ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8549c5cf-ac9d-4f42-b4a5-28c755f833ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3fe709f-74fd-4a92-a51e-eae8affc82ab",
                    "LayerId": "07ec653b-581d-4acc-8e60-11a0dc49bf98"
                }
            ]
        },
        {
            "id": "0715e3a7-a7b1-42a3-bc15-2bec2e568cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b55a47d-8551-423e-b66b-459e3f34b41a",
            "compositeImage": {
                "id": "7b29383e-b8eb-4f5e-b3f7-b2dd5ae29174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0715e3a7-a7b1-42a3-bc15-2bec2e568cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1da0dd7f-d064-413b-8024-5e64c4ab6abe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0715e3a7-a7b1-42a3-bc15-2bec2e568cb3",
                    "LayerId": "07ec653b-581d-4acc-8e60-11a0dc49bf98"
                }
            ]
        },
        {
            "id": "ad3ac312-9326-40cb-8523-5081e9a5b986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b55a47d-8551-423e-b66b-459e3f34b41a",
            "compositeImage": {
                "id": "e0dfa7c1-3bf5-43b5-bcbd-f95845fbec88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad3ac312-9326-40cb-8523-5081e9a5b986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95aaebb3-ebd9-491e-bffc-d75c781d9dde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad3ac312-9326-40cb-8523-5081e9a5b986",
                    "LayerId": "07ec653b-581d-4acc-8e60-11a0dc49bf98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "07ec653b-581d-4acc-8e60-11a0dc49bf98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b55a47d-8551-423e-b66b-459e3f34b41a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 15
}