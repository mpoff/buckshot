{
    "id": "efa528cc-395e-4d66-90ed-4a069a5ef204",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "frogatto_generic_bricks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8670f1e2-3adc-40e4-b5a2-bdb82250cca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efa528cc-395e-4d66-90ed-4a069a5ef204",
            "compositeImage": {
                "id": "07e71621-8df2-4321-b943-9b5d3377dbff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8670f1e2-3adc-40e4-b5a2-bdb82250cca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20ca4ff-803e-4569-9f99-e829457fc16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8670f1e2-3adc-40e4-b5a2-bdb82250cca5",
                    "LayerId": "cc953384-e62a-4f5a-b3e6-d7a169f729fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "cc953384-e62a-4f5a-b3e6-d7a169f729fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efa528cc-395e-4d66-90ed-4a069a5ef204",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}