{
    "id": "2f7c4074-c5b7-42d5-aa24-496655645ead",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9002a054-3e88-4781-a0bf-2c1c8ad81df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7c4074-c5b7-42d5-aa24-496655645ead",
            "compositeImage": {
                "id": "f60aa78d-09d1-49bb-af75-505f23e2fd9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9002a054-3e88-4781-a0bf-2c1c8ad81df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62541d28-d641-4c66-95a2-5a5d046d8b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9002a054-3e88-4781-a0bf-2c1c8ad81df2",
                    "LayerId": "acbf30dc-13b7-468f-b565-3542a27b1800"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "acbf30dc-13b7-468f-b565-3542a27b1800",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f7c4074-c5b7-42d5-aa24-496655645ead",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}