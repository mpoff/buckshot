{
    "id": "6aa3ed08-e368-4c12-be57-0980708bcfc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy2Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "349ac859-b8a5-4db3-82bc-a3a820a7219e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa3ed08-e368-4c12-be57-0980708bcfc2",
            "compositeImage": {
                "id": "9dac54f6-2e4c-43e6-ba3f-ac7f2bcc8876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349ac859-b8a5-4db3-82bc-a3a820a7219e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1489662-f8f1-44dd-a265-f23ef551429e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349ac859-b8a5-4db3-82bc-a3a820a7219e",
                    "LayerId": "a6fe3fcd-fe50-444a-a068-63ab0295308d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6fe3fcd-fe50-444a-a068-63ab0295308d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aa3ed08-e368-4c12-be57-0980708bcfc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 16
}