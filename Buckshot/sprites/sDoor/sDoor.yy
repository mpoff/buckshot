{
    "id": "478f5eb4-e775-465c-9473-12ae46a82529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1101947-08f5-44f0-9038-e3efd9898941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "478f5eb4-e775-465c-9473-12ae46a82529",
            "compositeImage": {
                "id": "e6f39d92-91cf-426a-84aa-b268cae68517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1101947-08f5-44f0-9038-e3efd9898941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a031ef-bb35-413d-9dc5-f9877f41d788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1101947-08f5-44f0-9038-e3efd9898941",
                    "LayerId": "cf867c67-176e-4a51-beba-167c6cc1ecce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cf867c67-176e-4a51-beba-167c6cc1ecce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "478f5eb4-e775-465c-9473-12ae46a82529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}