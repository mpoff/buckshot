{
    "id": "e67ebfbc-8821-4b44-9874-77f0cb236853",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16f4f05d-670b-424d-a8a1-1b1edfff5701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67ebfbc-8821-4b44-9874-77f0cb236853",
            "compositeImage": {
                "id": "d60eab41-10d3-4a2d-a008-86dcf9e0d0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f4f05d-670b-424d-a8a1-1b1edfff5701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a3ada6-f30e-4b98-b81c-b4b1a5ee12d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f4f05d-670b-424d-a8a1-1b1edfff5701",
                    "LayerId": "c70e1a89-58aa-4ac9-be5e-61a5889cdf14"
                }
            ]
        },
        {
            "id": "84c9c818-dc95-4222-b2be-6bf0717e4771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67ebfbc-8821-4b44-9874-77f0cb236853",
            "compositeImage": {
                "id": "8baf9a2f-09a5-46b8-99c3-6027eb832674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c9c818-dc95-4222-b2be-6bf0717e4771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e6c774-904b-4873-84f3-88593511692d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c9c818-dc95-4222-b2be-6bf0717e4771",
                    "LayerId": "c70e1a89-58aa-4ac9-be5e-61a5889cdf14"
                }
            ]
        },
        {
            "id": "6b6b5988-63fb-4348-acd7-10ca7e1703d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67ebfbc-8821-4b44-9874-77f0cb236853",
            "compositeImage": {
                "id": "16619f99-a650-4bb1-b80c-94905c7fe1f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6b5988-63fb-4348-acd7-10ca7e1703d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72cdedee-f9d6-4a09-b0ea-4410a191339b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6b5988-63fb-4348-acd7-10ca7e1703d1",
                    "LayerId": "c70e1a89-58aa-4ac9-be5e-61a5889cdf14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c70e1a89-58aa-4ac9-be5e-61a5889cdf14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e67ebfbc-8821-4b44-9874-77f0cb236853",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}