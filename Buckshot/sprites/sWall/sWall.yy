{
    "id": "8186fd68-b68b-48e1-8ccc-d81e147a2b2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79ab9634-ce26-4880-aa4a-4eec17ef5063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8186fd68-b68b-48e1-8ccc-d81e147a2b2f",
            "compositeImage": {
                "id": "0032112d-49d6-4db0-abb0-8c31d1cc88d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ab9634-ce26-4880-aa4a-4eec17ef5063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47735d65-b9ba-43c1-925d-ba5aef766b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ab9634-ce26-4880-aa4a-4eec17ef5063",
                    "LayerId": "66e23173-fbe9-495a-912a-31249ee3512a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "66e23173-fbe9-495a-912a-31249ee3512a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8186fd68-b68b-48e1-8ccc-d81e147a2b2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}