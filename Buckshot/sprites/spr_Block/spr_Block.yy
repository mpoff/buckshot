{
    "id": "01868f18-7fbb-4fdc-bc4f-843588999d61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Block",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc1c955b-b6f7-42a8-8af1-bfc1b9edd911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01868f18-7fbb-4fdc-bc4f-843588999d61",
            "compositeImage": {
                "id": "c904c8de-e67d-4ade-b6e0-e8237759742b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1c955b-b6f7-42a8-8af1-bfc1b9edd911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7c1eb2-6e92-40d8-af54-f1f3d99df508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1c955b-b6f7-42a8-8af1-bfc1b9edd911",
                    "LayerId": "03c65b81-bb48-4f18-a2e4-428ade86b365"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "03c65b81-bb48-4f18-a2e4-428ade86b365",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01868f18-7fbb-4fdc-bc4f-843588999d61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}