{
    "id": "ff20f0af-ea6a-4608-8ea8-8d220aadb3c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlideSlopeR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c71bae6c-cbb1-40e1-97f3-dc1561d5f007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff20f0af-ea6a-4608-8ea8-8d220aadb3c9",
            "compositeImage": {
                "id": "3431f255-3c0d-4d0b-9f80-8bc2bc73928e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71bae6c-cbb1-40e1-97f3-dc1561d5f007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b1e3e0-cae2-4426-99ee-ab9d52a81b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71bae6c-cbb1-40e1-97f3-dc1561d5f007",
                    "LayerId": "da42ffea-a227-4c69-b3d3-a72a6d16e57d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da42ffea-a227-4c69-b3d3-a72a6d16e57d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff20f0af-ea6a-4608-8ea8-8d220aadb3c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}