{
    "id": "375ea5d5-865d-4c09-85d1-b2cc37cfaea7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBounceH",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "903b439b-4d40-4aef-acfb-bd63fb1cd8e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "375ea5d5-865d-4c09-85d1-b2cc37cfaea7",
            "compositeImage": {
                "id": "f47e6ea8-8e51-4af2-8270-f4ca9b82e847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "903b439b-4d40-4aef-acfb-bd63fb1cd8e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35074ac5-69f3-4a4b-a255-f69cc36a5d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "903b439b-4d40-4aef-acfb-bd63fb1cd8e6",
                    "LayerId": "210a093d-b623-4f20-8a7f-f556f4432cb8"
                }
            ]
        },
        {
            "id": "c383198b-414f-4c43-a3ba-d4132526a2e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "375ea5d5-865d-4c09-85d1-b2cc37cfaea7",
            "compositeImage": {
                "id": "73b498dc-a7b9-40c6-8b6a-43869b808beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c383198b-414f-4c43-a3ba-d4132526a2e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec9a601-c1f9-42f8-af46-99f850b2f876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c383198b-414f-4c43-a3ba-d4132526a2e2",
                    "LayerId": "210a093d-b623-4f20-8a7f-f556f4432cb8"
                }
            ]
        },
        {
            "id": "e5a76155-3a7c-4388-bf1f-613e02b30d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "375ea5d5-865d-4c09-85d1-b2cc37cfaea7",
            "compositeImage": {
                "id": "cb9b79b5-de91-4cd3-bcd0-93659d61f16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a76155-3a7c-4388-bf1f-613e02b30d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804e9531-72ca-4aea-b31a-4f745d137382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a76155-3a7c-4388-bf1f-613e02b30d52",
                    "LayerId": "210a093d-b623-4f20-8a7f-f556f4432cb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "210a093d-b623-4f20-8a7f-f556f4432cb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "375ea5d5-865d-4c09-85d1-b2cc37cfaea7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 32
}