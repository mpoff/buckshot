{
    "id": "e4c4cf95-f730-4206-bb9b-5827fe6b406f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b63e8e6e-6351-41de-b08b-77fde3ba52d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4c4cf95-f730-4206-bb9b-5827fe6b406f",
            "compositeImage": {
                "id": "fbd3b2b5-be5c-4bb9-85ef-a84349c8c245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63e8e6e-6351-41de-b08b-77fde3ba52d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad6ccf3-4f3c-422d-b248-9d0a86960d48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63e8e6e-6351-41de-b08b-77fde3ba52d6",
                    "LayerId": "63a898d6-25f6-4523-92fb-039707c7d25f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "63a898d6-25f6-4523-92fb-039707c7d25f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4c4cf95-f730-4206-bb9b-5827fe6b406f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}