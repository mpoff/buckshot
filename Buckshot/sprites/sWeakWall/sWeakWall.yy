{
    "id": "0e2535c3-a9d2-4a53-8c57-974cfd6349c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWeakWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7076901-90de-4c38-9bf2-6a545cae3f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2535c3-a9d2-4a53-8c57-974cfd6349c9",
            "compositeImage": {
                "id": "c617c3aa-cec6-4a42-89a1-d6fbef504eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7076901-90de-4c38-9bf2-6a545cae3f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d80694-5b4b-431d-b704-af4ab56c07df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7076901-90de-4c38-9bf2-6a545cae3f60",
                    "LayerId": "0d11a5cd-4087-4981-8fa9-ddf58203343c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d11a5cd-4087-4981-8fa9-ddf58203343c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e2535c3-a9d2-4a53-8c57-974cfd6349c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}