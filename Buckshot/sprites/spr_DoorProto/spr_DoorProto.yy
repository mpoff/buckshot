{
    "id": "2917f57f-4946-4a18-9754-c179b6f9cf86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DoorProto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdb72fdd-aef1-4398-b8b8-f16ce25c882d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2917f57f-4946-4a18-9754-c179b6f9cf86",
            "compositeImage": {
                "id": "f201dee6-18df-4408-80a5-99f483a41340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb72fdd-aef1-4398-b8b8-f16ce25c882d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0baf9b3-6b41-4801-98ae-e18a53a2c9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb72fdd-aef1-4398-b8b8-f16ce25c882d",
                    "LayerId": "93247e69-0c1c-41c0-be1e-8c933060e085"
                }
            ]
        },
        {
            "id": "6e4ca709-a898-4f24-b0e5-cab2cb729ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2917f57f-4946-4a18-9754-c179b6f9cf86",
            "compositeImage": {
                "id": "9e197a02-d2be-4e78-a4ac-9a3cf481db2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e4ca709-a898-4f24-b0e5-cab2cb729ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50fe49d5-bf01-49a5-b871-e5e20d3ca195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e4ca709-a898-4f24-b0e5-cab2cb729ce1",
                    "LayerId": "93247e69-0c1c-41c0-be1e-8c933060e085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "93247e69-0c1c-41c0-be1e-8c933060e085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2917f57f-4946-4a18-9754-c179b6f9cf86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 63
}