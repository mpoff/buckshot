{
    "id": "36867e7e-4656-4563-892d-00a072e248d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0489aa8c-f6bd-4762-82ce-29e0942d84a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36867e7e-4656-4563-892d-00a072e248d9",
            "compositeImage": {
                "id": "34c2a39b-cc61-4244-bc4f-0f1783a602cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0489aa8c-f6bd-4762-82ce-29e0942d84a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef53fe3d-1dd8-4069-88f7-11dcbc454774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0489aa8c-f6bd-4762-82ce-29e0942d84a5",
                    "LayerId": "ecedcda3-e9cf-4350-b535-7c5348492621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ecedcda3-e9cf-4350-b535-7c5348492621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36867e7e-4656-4563-892d-00a072e248d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 36
}