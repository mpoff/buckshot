METRICS
bu = Buckshot unit
- 1bu = 1 meter = 1x1 sq of base sizing (16x16, 32x32, etc)
- Zander = 2bu tall, or 6m

FILE PREFIXES
Sprite: spr_FileName
Tile Sets: ts_FileName
Sounds: snd_FileName
Paths: pa_FileName
Scripts: scr_FileName
Shaders: sh_FileName
Fonts: fnt_FileName
Timelines: tl_FileName
Objects: obj_FileName
Rooms: rm_FileName

VERSION NUMBERING
[Major build number].[Minor build number].[Revision].[Package]

i.e. Version: 1.0.15.2

    Major build number: This indicates a major milestone in the game, increment this when going from beta to release, from release to major updates.
    Minor build number: Used for feature updates, large bug fixes etc.
    Revision: Minor alterations on existing features, small bug fixes, etc.
    Package: Your code stays the same, external library changes or asset file update.
