{
    "id": "dabbf7f8-f1fc-4473-8c86-2f885dc6972b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ButtonProto",
    "eventList": [
        {
            "id": "7691c817-b4d4-42a0-9c1e-a8336284abf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dabbf7f8-f1fc-4473-8c86-2f885dc6972b"
        },
        {
            "id": "d576d9ca-be8a-453f-8074-8c3ad06022f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dabbf7f8-f1fc-4473-8c86-2f885dc6972b"
        },
        {
            "id": "617fcc13-cfad-468f-9d55-d9a55508f529",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dabbf7f8-f1fc-4473-8c86-2f885dc6972b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "525247e9-ea4b-4b2d-b75f-488fe5ecc9a2",
    "visible": true
}