{
    "id": "1e14af0f-abd1-4470-a5fd-7edbe9cafeb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DoorProto",
    "eventList": [
        {
            "id": "623230d0-db96-4aec-94d6-9e4b833ea5a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e14af0f-abd1-4470-a5fd-7edbe9cafeb5"
        },
        {
            "id": "2bd78d8b-11d8-4ccd-8285-d936f10ea810",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1e14af0f-abd1-4470-a5fd-7edbe9cafeb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2917f57f-4946-4a18-9754-c179b6f9cf86",
    "visible": true
}