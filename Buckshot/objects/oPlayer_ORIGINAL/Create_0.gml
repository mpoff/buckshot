/// Tweak for room back. Check if we are in a new room or back to previous room
if global.prevRoom = true                           // If we got back to previous room..
{
    x = oExitDoor.x - sprite_width;                 
    y = oExitDoor.y + oExitDoor.sprite_height/2;    // ..our position is on fron of the ExitDoor
    global.prevRoom = false;
}

/// Init player

// ----- MOVEMENT VARS ---------------------------------- //
// Input control vars
var kLeft, kRight, kJump, kJumpRelease;
// Movement speed
h = 0;
v = 0;
// Max movement speed
maxH = 8.0;
// Jump and gravity variables
jumpHeight   = 10.0;
gravityForce = 0.80;
maxFallSpeed = 8.0;
// Check collisions below
onGround = false;
// ----------------------------------------------------- //

// ----- PLAYER STATES INFO ---------------------------- //
// States
IDLE     = 10;
RUN      = 11;
JUMP     = 12;
DEAD     = 13;
// Facings
RIGHT =  1;
LEFT  = -1;
// Initialize player's properties
state  = IDLE;
facing = RIGHT;
// ---------------------------------------------------- //

// ------ GAME CAMERA ----------------------------------- //

// Create the Camera that will follow the player and set a camera speed
// In GMS:2, cameras can be easily set on the "Viewports and Cameras" settings of our room...
// ... for thos interested here we will creat and init our camera in Game Maker Language here, we will only init the camera position on Player position every time we change a room
view_camera[0] = camera_create_view(0, 0, view_wport[0], view_hport[0], 0, self, -1, -1, 320, 64);
// ------------------------------------------------------ //

