/// Reach the exit door

// If we reach an exit door, we go to the next room (if there are any next room!)
if room_next(room) != -1
{
    room_goto_next();
}

