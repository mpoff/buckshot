{
    "id": "f2567600-28e5-4762-a717-ed046c49b1c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer_ORIGINAL",
    "eventList": [
        {
            "id": "39112b82-ec29-44e2-a610-81421f2b24e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        },
        {
            "id": "9e358b20-f6de-40c0-aacc-712d377e4308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        },
        {
            "id": "e18ae6dd-84f1-4a1d-9141-97d3c373ac7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        },
        {
            "id": "8a3de059-b948-4371-97ed-5adbb6b7a892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        },
        {
            "id": "be1140b0-c6b7-408e-bb40-6fdc36f56a97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffd18a2f-0820-46e4-bc8e-0e1da0e434a9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        },
        {
            "id": "be286d73-dc37-4b16-aa4b-d86405fc2014",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f2567600-28e5-4762-a717-ed046c49b1c7"
        }
    ],
    "maskSpriteId": "2f7c4074-c5b7-42d5-aa24-496655645ead",
    "overriddenProperties": null,
    "parentObjectId": "27b0d8f7-6ccf-42da-8b29-3b91bda1616e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
    "visible": true
}