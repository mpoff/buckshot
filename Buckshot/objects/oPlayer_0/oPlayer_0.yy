{
    "id": "e04ac9f9-1188-4855-906a-fb43440173fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer_0",
    "eventList": [
        {
            "id": "b2cc5390-c5df-4e01-a33a-6add289c53c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        },
        {
            "id": "b995f5f8-912b-4358-b314-5ad4e061d1e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        },
        {
            "id": "3846f867-c309-4546-ad43-008692c6e05c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        },
        {
            "id": "9eefe4d5-ecc2-4ae1-b692-91026ce5c21b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        },
        {
            "id": "7438b00c-80b7-4c45-ab8f-c57da7fad34c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffd18a2f-0820-46e4-bc8e-0e1da0e434a9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        },
        {
            "id": "e542e072-72dc-4855-90c4-57f1df65e819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e04ac9f9-1188-4855-906a-fb43440173fb"
        }
    ],
    "maskSpriteId": "2f7c4074-c5b7-42d5-aa24-496655645ead",
    "overriddenProperties": null,
    "parentObjectId": "27b0d8f7-6ccf-42da-8b29-3b91bda1616e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
    "visible": true
}