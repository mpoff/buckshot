/// @description Draw info texts
/// What has this template?

draw_set_alpha(0.25);
var x1 = camera_get_view_x(view_camera[0]) + 10;
var y1 = camera_get_view_y(view_camera[0]) + 10;
var x2 = x1 + 360;
var y2 = y1 + 120;
draw_rectangle_colour(x1, y1, x2, y2, c_white, c_white, c_white, c_white, false); 
draw_set_alpha(1);
draw_text(x1 + 10, y1 + 10, string_hash_to_newline("BASIC PLATFORM GAMEPLAY"));
draw_text(x1 + 10, y1 + 30, string_hash_to_newline("- Basic collisions"));
draw_text(x1 + 10, y1 + 50, string_hash_to_newline("- Player's jump and gravity"));
draw_text(x1 + 10, y1 + 70, string_hash_to_newline("- Game camera with boundaries"));
draw_text(x1 + 10, y1 + 90, string_hash_to_newline("- Keyboard an Pad controls in 1 script"));

