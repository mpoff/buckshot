/// Reached the exit door

// If we reach an exit door, we go to the next room (if there are any next room!)
if room_next(room) != -1
{
    room_goto_next();
}
else
{
    show_message("Demo end. Try full version for more complete templates!");
	url_open("https://marketplace.yoyogames.com/assets/2982/platform-gameplay-templates");
    game_end();
}

