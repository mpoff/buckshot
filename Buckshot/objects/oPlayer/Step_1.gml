/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7902461F
/// @DnDArgument : "code" "/// Previous frame checks$(13_10)$(13_10)// Check if we're on the ground$(13_10)onGround = place_meeting(x,y+1,oParSolid);$(13_10)$(13_10)// Update relative collision$(13_10)colLeft    = place_meeting(x - 1, y, oParSolid)$(13_10)colRight   = place_meeting(x + 1, y, oParSolid)$(13_10)$(13_10)// Idle state always if anything changes$(13_10)state = IDLE;"
/// Previous frame checks

// Check if we're on the ground
onGround = place_meeting(x,y+1,oParSolid);

// Update relative collision
colLeft    = place_meeting(x - 1, y, oParSolid)
colRight   = place_meeting(x + 1, y, oParSolid)

// Idle state always if anything changes
state = IDLE;