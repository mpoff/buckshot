/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 20DE5852
/// @DnDArgument : "code" "/// Movement$(13_10)$(13_10)// Check input controls (for keyboard and joypad)$(13_10)checkInput();$(13_10)$(13_10)// Gravity$(13_10)$(13_10)if (!onGround) {$(13_10)	//Set the sprite to be jumping$(13_10)    state = JUMP;$(13_10)	$(13_10)    if ((colLeft or colRight) and v >= 0) $(13_10)    {$(13_10)        v = lerp(v, maxV, gravSlide);           // Gravity on Wall slide$(13_10)    } $(13_10)    else $(13_10)    {$(13_10)    // Make player fall$(13_10)    if (v < 0)$(13_10)        v = lerp(v, maxV, gravRise);            // Gravity on rise$(13_10)    else$(13_10)        v = lerp(v, maxV, gravFall);            // Gravity on fall$(13_10)    }$(13_10)}$(13_10)$(13_10)$(13_10)// Left $(13_10)//If pressing left move key and not right move key...$(13_10)if (kLeft and !kRight)$(13_10){$(13_10)	//Set the sprite to be facing left$(13_10)    facing = LEFT;$(13_10)	//If you're moving...$(13_10)    if (h > 0)$(13_10)		//Slide and brake if change direccion$(13_10)        h = lerp(h, 0, groundFric);				$(13_10)	//Apply left acceleration$(13_10)    h = lerp(h, -maxH, groundAccel);				$(13_10)        $(13_10)	// If touch ground, show Run animation$(13_10)    if (onGround)$(13_10)        state = RUN;                                $(13_10)}$(13_10)$(13_10)// Right $(13_10)if (kRight and !kLeft)$(13_10){$(13_10)    facing = RIGHT;                                 // Player looks on the right$(13_10)    if (h < 0)$(13_10)        h = lerp(h, 0, groundFric);					// Slide and brake if change direccion$(13_10)    h = lerp(h, maxH, groundAccel);					// Apply right acceleration$(13_10)        $(13_10)    if (onGround)$(13_10)        state = RUN;                                // If touch ground, show Run animation$(13_10)}$(13_10)$(13_10)// No moving inputs$(13_10)if (!kRight and !kLeft)$(13_10)    h = lerp(h, 0, groundFric);						// Slide and brake when not moving$(13_10)    $(13_10)// Jump$(13_10)/*$(13_10)if (kJump && onGround)                              // If jump and touching ground..$(13_10){$(13_10)    v = -jumpHeight;                                // ..give jump momentum to player..$(13_10)    state = JUMP;                                   // ..and show jump animation$(13_10)} $(13_10)else $(13_10){$(13_10)    if (kJumpRelease)                               // If stop pressing jump key..$(13_10)    {$(13_10)        if (v < 0)$(13_10)            v *= 0.25;                              // ..rapidly slow down our jump momentum and let gravity do its job!$(13_10)    }$(13_10)}$(13_10)*/$(13_10)// ------ CAMERA UPDATES ----------$(13_10)// Make camera follows Player with a small smooth panning$(13_10)var hDir = keyboard_check(vk_right) - keyboard_check(vk_left);		// Nice trick to know which lateral direction are we facing$(13_10)$(13_10)var _viewX = camera_get_view_x(view_camera[0]);						// Get camera X position$(13_10)var _viewY = camera_get_view_y(view_camera[0]);						// Get camera Y position$(13_10)var _viewW = camera_get_view_width(view_camera[0]);					// Get camera width$(13_10)var _viewH = camera_get_view_height(view_camera[0]);				// Get camera height$(13_10)$(13_10)// When moving we will displace the camera a bit from player to show more of whats in front of him$(13_10)var dispX = x + (hDir * 150) - (_viewW/2);							// Displacement of the camera on lateral movement$(13_10)var dispY = y - (_viewH/2);											// There won't be vertiical displacement. Camera will be always centered to the Player$(13_10)$(13_10)// Look at YoYo Docs what clam() and lerp() are for!$(13_10)// We use clamp() here to avoid camera show further from our room limits$(13_10)// ... and lerp() to make an smooth progression between the camera actual position to its final position depending on player's position$(13_10)var _newX = clamp(lerp(_viewX, dispX, 0.05), 0, room_width - _viewW);$(13_10)var _newY = clamp(lerp(_viewY, dispY, 0.5), 0, room_height - _viewH);$(13_10)$(13_10)// Finally we update our camera position$(13_10)camera_set_view_pos(view_camera[0], _newX, _newY);$(13_10)$(13_10)$(13_10)"
/// Movement

// Check input controls (for keyboard and joypad)
checkInput();

// Gravity

if (!onGround) {
	//Set the sprite to be jumping
    state = JUMP;
	
    if ((colLeft or colRight) and v >= 0) 
    {
        v = lerp(v, maxV, gravSlide);           // Gravity on Wall slide
    } 
    else 
    {
    // Make player fall
    if (v < 0)
        v = lerp(v, maxV, gravRise);            // Gravity on rise
    else
        v = lerp(v, maxV, gravFall);            // Gravity on fall
    }
}


// Left 
//If pressing left move key and not right move key...
if (kLeft and !kRight)
{
	//Set the sprite to be facing left
    facing = LEFT;
	//If you're moving...
    if (h > 0)
		//Slide and brake if change direccion
        h = lerp(h, 0, groundFric);				
	//Apply left acceleration
    h = lerp(h, -maxH, groundAccel);				
        
	// If touch ground, show Run animation
    if (onGround)
        state = RUN;                                
}

// Right 
if (kRight and !kLeft)
{
    facing = RIGHT;                                 // Player looks on the right
    if (h < 0)
        h = lerp(h, 0, groundFric);					// Slide and brake if change direccion
    h = lerp(h, maxH, groundAccel);					// Apply right acceleration
        
    if (onGround)
        state = RUN;                                // If touch ground, show Run animation
}

// No moving inputs
if (!kRight and !kLeft)
    h = lerp(h, 0, groundFric);						// Slide and brake when not moving
    
// Jump
/*
if (kJump && onGround)                              // If jump and touching ground..
{
    v = -jumpHeight;                                // ..give jump momentum to player..
    state = JUMP;                                   // ..and show jump animation
} 
else 
{
    if (kJumpRelease)                               // If stop pressing jump key..
    {
        if (v < 0)
            v *= 0.25;                              // ..rapidly slow down our jump momentum and let gravity do its job!
    }
}
*/
// ------ CAMERA UPDATES ----------
// Make camera follows Player with a small smooth panning
var hDir = keyboard_check(vk_right) - keyboard_check(vk_left);		// Nice trick to know which lateral direction are we facing

var _viewX = camera_get_view_x(view_camera[0]);						// Get camera X position
var _viewY = camera_get_view_y(view_camera[0]);						// Get camera Y position
var _viewW = camera_get_view_width(view_camera[0]);					// Get camera width
var _viewH = camera_get_view_height(view_camera[0]);				// Get camera height

// When moving we will displace the camera a bit from player to show more of whats in front of him
var dispX = x + (hDir * 150) - (_viewW/2);							// Displacement of the camera on lateral movement
var dispY = y - (_viewH/2);											// There won't be vertiical displacement. Camera will be always centered to the Player

// Look at YoYo Docs what clam() and lerp() are for!
// We use clamp() here to avoid camera show further from our room limits
// ... and lerp() to make an smooth progression between the camera actual position to its final position depending on player's position
var _newX = clamp(lerp(_viewX, dispX, 0.05), 0, room_width - _viewW);
var _newY = clamp(lerp(_viewY, dispY, 0.5), 0, room_height - _viewH);

// Finally we update our camera position
camera_set_view_pos(view_camera[0], _newX, _newY);


/**/