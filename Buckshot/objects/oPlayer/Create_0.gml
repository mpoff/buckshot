/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 682B3B08
/// @DnDArgument : "code" "/// Tweak for room back. Check if we are in a new room or back to previous room$(13_10)if global.prevRoom = true                           // If we got back to previous room..$(13_10){$(13_10)    x = oExitDoor.x - sprite_width;                 $(13_10)    y = oExitDoor.y + oExitDoor.sprite_height/2;    // ..our position is on fron of the ExitDoor$(13_10)    global.prevRoom = false;$(13_10)}$(13_10)$(13_10)/// Init player$(13_10)$(13_10)// ------------------------------------------------------ //$(13_10)$(13_10)// ----- MOVEMENT VARS ---------------------------------- //$(13_10)// Input control vars$(13_10)var kLeft, kRight, kUp, kDown; //kJump, kJumpRelease;$(13_10)// Movement speed$(13_10)h = 0;$(13_10)v = 0;$(13_10)// Acceleration + friction$(13_10)groundAccel = 0.5;$(13_10)groundFric  = 0.5;$(13_10)// Max movement speeds$(13_10)maxH        = 8.0;$(13_10)maxV        = 10.0;$(13_10)// Jump and gravity variables$(13_10)jumpHeight  = 10.0;$(13_10)gravRise    = 0.05;						// Gravity value while jumping up$(13_10)gravFall    = 0.20;							// Gravity value while falling$(13_10)gravSlide   = 0.75;							// Gravity value on wall sliding$(13_10)wJumpPlus   = 3.0;                          // Jump force between walls$(13_10)gripTime	= room_speed;                   // Time we have to jump again on a wall$(13_10)// ----------------------------------------------------- //$(13_10)$(13_10)// ----- PLAYER STATES INFO ---------------------------- //$(13_10)// States$(13_10)IDLE     = 10;$(13_10)RUN      = 11;$(13_10)JUMP     = 12;$(13_10)DEAD     = 13;$(13_10)// Facings$(13_10)RIGHT =  1;$(13_10)LEFT  = -1;$(13_10)// Initialize player's properties$(13_10)state  = IDLE;$(13_10)facing = RIGHT;$(13_10)// ---------------------------------------------------- //$(13_10)$(13_10)// ------ GAME CAMERA ----------------------------------- //$(13_10)$(13_10)// Create the Camera that will follow the player and set a camera speed$(13_10)// In GMS:2, cameras can be easily set on the "Viewports and Cameras" settings of our room...$(13_10)// ... for thos interested here we will creat and init our camera in Game Maker Language here, we will only init the camera position on Player position every time we change a room$(13_10)view_camera[0] = camera_create_view(0, 0, view_wport[0], view_hport[0], 0, self, -1, -1, -1, -1);$(13_10)// ------------------------------------------------------ //$(13_10)$(13_10)"
/// Tweak for room back. Check if we are in a new room or back to previous room
if global.prevRoom = true                           // If we got back to previous room..
{
    x = oExitDoor.x - sprite_width;                 
    y = oExitDoor.y + oExitDoor.sprite_height/2;    // ..our position is on fron of the ExitDoor
    global.prevRoom = false;
}

/// Init player

// ------------------------------------------------------ //

// ----- MOVEMENT VARS ---------------------------------- //
// Input control vars
var kLeft, kRight, kUp, kDown; //kJump, kJumpRelease;
// Movement speed
h = 0;
v = 0;
// Acceleration + friction
groundAccel = 0.5;
groundFric  = 0.5;
// Max movement speeds
maxH        = 8.0;
maxV        = 10.0;
// Jump and gravity variables
jumpHeight  = 10.0;
gravRise    = 0.05;						// Gravity value while jumping up
gravFall    = 0.20;							// Gravity value while falling
gravSlide   = 0.75;							// Gravity value on wall sliding
wJumpPlus   = 3.0;                          // Jump force between walls
gripTime	= room_speed;                   // Time we have to jump again on a wall
// ----------------------------------------------------- //

// ----- PLAYER STATES INFO ---------------------------- //
// States
IDLE     = 10;
RUN      = 11;
JUMP     = 12;
DEAD     = 13;
// Facings
RIGHT =  1;
LEFT  = -1;
// Initialize player's properties
state  = IDLE;
facing = RIGHT;
// ---------------------------------------------------- //

// ------ GAME CAMERA ----------------------------------- //

// Create the Camera that will follow the player and set a camera speed
// In GMS:2, cameras can be easily set on the "Viewports and Cameras" settings of our room...
// ... for thos interested here we will creat and init our camera in Game Maker Language here, we will only init the camera position on Player position every time we change a room
view_camera[0] = camera_create_view(0, 0, view_wport[0], view_hport[0], 0, self, -1, -1, -1, -1);
// ------------------------------------------------------ //

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 6610956A
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "obj_Shotgun"
/// @DnDArgument : "layer" ""Items""
/// @DnDSaveInfo : "objectid" "908364f3-f961-47db-9f85-44d9293db54b"
instance_create_layer(x + 0, y + 0, "Items", obj_Shotgun);