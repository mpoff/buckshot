{
    "id": "c89abf71-c430-4c00-8189-a8eb140f4be6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "aa780d6c-8f72-475f-bb9a-ad3b2d8e78ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        },
        {
            "id": "9a5eff20-4697-4035-8ff2-2156072a1a6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        },
        {
            "id": "6c9763fd-ccc4-415b-b715-05cb194853fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        },
        {
            "id": "a8dd7c4f-097e-4b1c-9563-82e521d02cc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        },
        {
            "id": "07330434-99b1-4d99-ab87-a0c0b819be24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffd18a2f-0820-46e4-bc8e-0e1da0e434a9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        },
        {
            "id": "e961ee4a-b1f7-48ce-ab87-c800ef081efd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "de79f78a-6a12-401c-8e57-7d7a04c78c3c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c89abf71-c430-4c00-8189-a8eb140f4be6"
        }
    ],
    "maskSpriteId": "2f7c4074-c5b7-42d5-aa24-496655645ead",
    "overriddenProperties": null,
    "parentObjectId": "27b0d8f7-6ccf-42da-8b29-3b91bda1616e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c11de5d6-9fda-401d-ac3e-e8a1677b5e00",
    "visible": true
}