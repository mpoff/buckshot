/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 24C6D3B7
/// @DnDArgument : "code" "/// Actual collision checks and properties updates$(13_10)$(13_10)//Update state properties$(13_10)image_xscale = facing;                                      // Player sprite always looks on facing direction$(13_10)$(13_10)// COLLISIONS AND POSITION UPDATE ------------------------- //$(13_10)// Vertical$(13_10)repeat(abs(v)) {$(13_10)    if (!place_meeting(x, y + sign(v), oParSolid))          // If player don't collide under or above..$(13_10)        y += sign(v);                                       // ..update y position$(13_10)    else {                                                  // BUT if he collides..$(13_10)        v = 0;                                              // ..can't go further..$(13_10)        break;                                              // ..and we stop checking vertical collisions this frame$(13_10)    }$(13_10)}$(13_10)// Horizontal$(13_10)repeat(abs(h)) {  $(13_10)    if (!place_meeting(x + sign(h), y, oParSolid))          // If player don't collide left or right..$(13_10)        x += sign(h);                                       // ..update x position$(13_10)    else {                                                  // BUT if he collides..$(13_10)        h = 0;                                              // ..can't go further..$(13_10)        break;                                              // ..and we stop checking horizontal collisions this frame$(13_10)    }$(13_10)}$(13_10)// -------------------------------------------------------- //$(13_10)$(13_10)// ------ STATES MACHINE ---------------------------------- //$(13_10)switch (state) {$(13_10)    case IDLE: $(13_10)        image_speed = 0.15;$(13_10)        sprite_index = sPlayerIdle;$(13_10)    break;$(13_10)    $(13_10)    case RUN: $(13_10)        image_speed = 0.5; $(13_10)        sprite_index = sPlayerRun;$(13_10)    break;$(13_10)    $(13_10)    case JUMP:$(13_10)        image_speed = 0.5; $(13_10)        // Jump and fall$(13_10)        if (v <= 0) $(13_10)            sprite_index = sPlayerJump;  $(13_10)        else$(13_10)            sprite_index = sPlayerFall;$(13_10)            $(13_10)        if image_index >= image_number-1$(13_10)            image_speed = 0;$(13_10)    break;$(13_10)}$(13_10)// -------------------------------------------------------- //$(13_10)$(13_10)// ------ WIN AND LOOSE CONDITIONS ------------------------ //$(13_10)// If player falls off the room, "dies" (we restart the room)$(13_10)//if (y > room_height + sprite_height/2)$(13_10)//    room_restart();$(13_10)// -------------------------------------------------------- //$(13_10)$(13_10)"
/// Actual collision checks and properties updates

//Update state properties
image_xscale = facing;                                      // Player sprite always looks on facing direction

// COLLISIONS AND POSITION UPDATE ------------------------- //
// Vertical
repeat(abs(v)) {
    if (!place_meeting(x, y + sign(v), oParSolid))          // If player don't collide under or above..
        y += sign(v);                                       // ..update y position
    else {                                                  // BUT if he collides..
        v = 0;                                              // ..can't go further..
        break;                                              // ..and we stop checking vertical collisions this frame
    }
}
// Horizontal
repeat(abs(h)) {  
    if (!place_meeting(x + sign(h), y, oParSolid))          // If player don't collide left or right..
        x += sign(h);                                       // ..update x position
    else {                                                  // BUT if he collides..
        h = 0;                                              // ..can't go further..
        break;                                              // ..and we stop checking horizontal collisions this frame
    }
}
// -------------------------------------------------------- //

// ------ STATES MACHINE ---------------------------------- //
switch (state) {
    case IDLE: 
        image_speed = 0.15;
        sprite_index = sPlayerIdle;
    break;
    
    case RUN: 
        image_speed = 0.5; 
        sprite_index = sPlayerRun;
    break;
    
    case JUMP:
        image_speed = 0.5; 
        // Jump and fall
        if (v <= 0) 
            sprite_index = sPlayerJump;  
        else
            sprite_index = sPlayerFall;
            
        if image_index >= image_number-1
            image_speed = 0;
    break;
}
// -------------------------------------------------------- //

// ------ WIN AND LOOSE CONDITIONS ------------------------ //
// If player falls off the room, "dies" (we restart the room)
//if (y > room_height + sprite_height/2)
//    room_restart();
// -------------------------------------------------------- //