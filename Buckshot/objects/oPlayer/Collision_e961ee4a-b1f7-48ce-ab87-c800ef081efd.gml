/// Cross entrance door

// If we pass through an exit door, we go to the previous room
if room_previous(room) != -1
{
    global.prevRoom = true;
    room_goto_previous();
}

