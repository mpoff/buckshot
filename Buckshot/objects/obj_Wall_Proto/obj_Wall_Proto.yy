{
    "id": "92157566-35dc-4396-bf14-c376bd053416",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Wall_Proto",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cadfb89b-1251-41ec-a5d3-43057fd3778c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "01868f18-7fbb-4fdc-bc4f-843588999d61",
    "visible": true
}