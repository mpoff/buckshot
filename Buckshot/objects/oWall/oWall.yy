{
    "id": "a7953ab4-50a4-4efa-b4bb-5e02ac458e56",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWall",
    "eventList": [
        {
            "id": "56a763ab-4580-4637-8538-feac2d0f6fc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7953ab4-50a4-4efa-b4bb-5e02ac458e56"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "65efedb0-12c4-4173-b1cd-660cc989a282",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8186fd68-b68b-48e1-8ccc-d81e147a2b2f",
    "visible": true
}