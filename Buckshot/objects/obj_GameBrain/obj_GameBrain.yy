{
    "id": "3f33f9d7-0d1f-462b-82e5-fdc6aef0a922",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GameBrain",
    "eventList": [
        {
            "id": "1b03d3c4-62dd-450d-9950-7c1460cb49c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f33f9d7-0d1f-462b-82e5-fdc6aef0a922"
        },
        {
            "id": "c528a19b-3521-407c-9b02-601c18f95268",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "3f33f9d7-0d1f-462b-82e5-fdc6aef0a922"
        },
        {
            "id": "b15d0418-d4a5-4d2a-b7d2-9daa28543c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "3f33f9d7-0d1f-462b-82e5-fdc6aef0a922"
        },
        {
            "id": "01d6278a-a208-419d-8085-0c52a8a4e5ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 45,
            "eventtype": 9,
            "m_owner": "3f33f9d7-0d1f-462b-82e5-fdc6aef0a922"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}