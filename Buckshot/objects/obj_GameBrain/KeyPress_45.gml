/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 76ED6442
/// @DnDArgument : "code" "//open $(13_10)var cmd_input = get_string("Enter command:","");"
//open 
var cmd_input = get_string("Enter command:","");

/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 082BD5E6
/// @DnDArgument : "expr" "cmd_input"
var l082BD5E6_0 = cmd_input;
switch(l082BD5E6_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 358EABE5
	/// @DnDParent : 082BD5E6
	/// @DnDArgument : "const" ""reset_game""
	case "reset_game":
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 197920FE
		/// @DnDParent : 358EABE5
		game_restart();
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 549B1641
	/// @DnDParent : 082BD5E6
	/// @DnDArgument : "const" ""inf_ammo""
	case "inf_ammo":
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3C13188C
		/// @DnDParent : 549B1641
		/// @DnDArgument : "expr" "999"
		/// @DnDArgument : "var" "obj_Shotgun.max_ammo"
		obj_Shotgun.max_ammo = 999;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2C35EC5B
		/// @DnDParent : 549B1641
		/// @DnDArgument : "expr" "999"
		/// @DnDArgument : "var" "obj_Shotgun.ammo"
		obj_Shotgun.ammo = 999;
		break;
}