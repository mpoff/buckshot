{
    "id": "ef187b88-6fba-4401-b966-10b776925c43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bullet",
    "eventList": [
        {
            "id": "c9fe45b2-4e6a-4ca1-9870-475c16343fa9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef187b88-6fba-4401-b966-10b776925c43"
        },
        {
            "id": "b3ed4063-9e50-446e-bf8c-759308629999",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "ef187b88-6fba-4401-b966-10b776925c43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4c4cf95-f730-4206-bb9b-5827fe6b406f",
    "visible": true
}