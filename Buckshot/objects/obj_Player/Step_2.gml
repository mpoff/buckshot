/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2A4E7B5F
/// @DnDArgument : "code" "//Set sprite to face correct direction$(13_10)if sprite_facing == 1 {$(13_10)	sprite_xscale = 1;$(13_10)} else {$(13_10)	sprite_xscale = -1;$(13_10)}"
//Set sprite to face correct direction
if sprite_facing == 1 {
	sprite_xscale = 1;
} else {
	sprite_xscale = -1;
}