/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 70797604
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "obj_Shotgun"
/// @DnDArgument : "layer" ""Items""
/// @DnDSaveInfo : "objectid" "908364f3-f961-47db-9f85-44d9293db54b"
instance_create_layer(x + 0, y + 0, "Items", obj_Shotgun);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6799BED0
/// @DnDArgument : "code" "/// @description Init Vars$(13_10)$(13_10)// Player Vars$(13_10)move_rate_ground = 1;$(13_10)move_rate_air = 20;$(13_10)jump_rate = 4;$(13_10)jump_buffer_count = 0;$(13_10)jump_buffer = 10;$(13_10)jump_ledge_buffer = 10;$(13_10)accel_rate_ground = 0.03;$(13_10)accel_rate_air = 0.2;$(13_10)brake_rate_ground = 0.4;$(13_10)brake_rate_air = 1;$(13_10)gravity_vspeed = 10;$(13_10)gravity_rate = 0.2;$(13_10)$(13_10)// Controls$(13_10)control_left = ord("A");$(13_10)control_right = ord("D");$(13_10)//control_jump = vk_space;$(13_10)// Alternate Controls$(13_10)control_left_alt = vk_left;$(13_10)control_right_alt = vk_right;$(13_10)//control_jump_alt = vk_up;$(13_10)$(13_10)//ANIMATION STATES$(13_10)// 0 = idle$(13_10)// 1 = ground run$(13_10)// 2 = air up$(13_10)// 3 = air down$(13_10)anim_state = 0;"
/// @description Init Vars

// Player Vars
move_rate_ground = 1;
move_rate_air = 20;
jump_rate = 4;
jump_buffer_count = 0;
jump_buffer = 10;
jump_ledge_buffer = 10;
accel_rate_ground = 0.03;
accel_rate_air = 0.2;
brake_rate_ground = 0.4;
brake_rate_air = 1;
gravity_vspeed = 10;
gravity_rate = 0.2;

// Controls
control_left = ord("A");
control_right = ord("D");
//control_jump = vk_space;
// Alternate Controls
control_left_alt = vk_left;
control_right_alt = vk_right;
//control_jump_alt = vk_up;

//ANIMATION STATES
// 0 = idle
// 1 = ground run
// 2 = air up
// 3 = air down
anim_state = 0;