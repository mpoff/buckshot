/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5A5B991A
/// @DnDArgument : "code" "/// @description Control Player$(13_10)$(13_10)// Get input$(13_10)move_input_total = 0;$(13_10)if keyboard_check(control_left) || keyboard_check(control_left_alt) { $(13_10)	image_xscale = -1;$(13_10)	move_input_total -= 1; $(13_10)	anim_state = 1;//run sprite$(13_10)}$(13_10)if keyboard_check(control_right) || keyboard_check(control_right_alt) {$(13_10)	image_xscale = 1;$(13_10)	move_input_total += 1; $(13_10)	anim_state = 1;//run sprite$(13_10)}$(13_10)$(13_10)if place_meeting(x, y + 1, obj_SOLID)$(13_10){$(13_10)   if move_input_total == 0 || (hspeed * move_input_total < 0)$(13_10)   {$(13_10)       hspeed -= hspeed * brake_rate_ground;$(13_10)   }$(13_10)}$(13_10)// In air$(13_10)$(13_10)if !place_meeting(x, y + 1, obj_SOLID) && hspeed >= move_rate_air$(13_10){$(13_10)	anim_state = 2;//air up sprite$(13_10)	hspeed -= hspeed * brake_rate_air;$(13_10)   $(13_10)}$(13_10)$(13_10)$(13_10)// Move player and clamp value to max$(13_10)// On ground$(13_10)if place_meeting(x, y + 1, obj_SOLID)$(13_10){$(13_10)	hspeed += move_input_total * accel_rate_ground;$(13_10)	hspeed = clamp(hspeed, -move_rate_ground, move_rate_ground);$(13_10)}$(13_10)$(13_10)// In air$(13_10)if !place_meeting(x, y + 1, obj_SOLID)$(13_10){$(13_10)	anim_state = 2;//air up sprite$(13_10)	hspeed += move_input_total * accel_rate_air;$(13_10)}$(13_10)$(13_10)// Gravity$(13_10)if (vspeed < gravity_vspeed) || !place_meeting(x, y + 1, obj_SOLID)$(13_10){$(13_10)	vspeed += gravity_rate;$(13_10)}$(13_10)$(13_10)// Collisions and stuck/overlap prevention$(13_10)if (place_meeting(x + hspeed, y, obj_SOLID)) {$(13_10)	while (!place_meeting(x + sign(hspeed), y, obj_SOLID)) {$(13_10)		x += sign(hspeed);$(13_10)	}$(13_10)	hspeed = 0;$(13_10)}$(13_10)if (place_meeting(x, y + vspeed, obj_SOLID)) {$(13_10)	while (!place_meeting(x, y + sign(vspeed), obj_SOLID)) {$(13_10)		y += sign(vspeed);$(13_10)	}$(13_10)	vspeed = 0;$(13_10)}$(13_10)// Diagonal$(13_10)if (place_meeting(x + hspeed, y + vspeed, obj_SOLID)) {$(13_10)	while (!place_meeting(x + sign(hspeed), y + sign(vspeed), obj_SOLID)) {$(13_10)		x += sign(hspeed);$(13_10)		y += sign(vspeed);$(13_10)	}$(13_10)	hspeed = 0;$(13_10)	vspeed = 0;$(13_10)}$(13_10)$(13_10)if move_input_total == 0 && hspeed == 0 && vspeed == 0{$(13_10)	anim_state = 0;$(13_10)}$(13_10)$(13_10)if vspeed > 0 {$(13_10)	anim_state = 3;	$(13_10)}"
/// @description Control Player

// Get input
move_input_total = 0;
if keyboard_check(control_left) || keyboard_check(control_left_alt) { 
	image_xscale = -1;
	move_input_total -= 1; 
	anim_state = 1;//run sprite
}
if keyboard_check(control_right) || keyboard_check(control_right_alt) {
	image_xscale = 1;
	move_input_total += 1; 
	anim_state = 1;//run sprite
}

if place_meeting(x, y + 1, obj_SOLID)
{
   if move_input_total == 0 || (hspeed * move_input_total < 0)
   {
       hspeed -= hspeed * brake_rate_ground;
   }
}
// In air

if !place_meeting(x, y + 1, obj_SOLID) && hspeed >= move_rate_air
{
	anim_state = 2;//air up sprite
	hspeed -= hspeed * brake_rate_air;
   
}


// Move player and clamp value to max
// On ground
if place_meeting(x, y + 1, obj_SOLID)
{
	hspeed += move_input_total * accel_rate_ground;
	hspeed = clamp(hspeed, -move_rate_ground, move_rate_ground);
}

// In air
if !place_meeting(x, y + 1, obj_SOLID)
{
	anim_state = 2;//air up sprite
	hspeed += move_input_total * accel_rate_air;
}

// Gravity
if (vspeed < gravity_vspeed) || !place_meeting(x, y + 1, obj_SOLID)
{
	vspeed += gravity_rate;
}

// Collisions and stuck/overlap prevention
if (place_meeting(x + hspeed, y, obj_SOLID)) {
	while (!place_meeting(x + sign(hspeed), y, obj_SOLID)) {
		x += sign(hspeed);
	}
	hspeed = 0;
}
if (place_meeting(x, y + vspeed, obj_SOLID)) {
	while (!place_meeting(x, y + sign(vspeed), obj_SOLID)) {
		y += sign(vspeed);
	}
	vspeed = 0;
}
// Diagonal
if (place_meeting(x + hspeed, y + vspeed, obj_SOLID)) {
	while (!place_meeting(x + sign(hspeed), y + sign(vspeed), obj_SOLID)) {
		x += sign(hspeed);
		y += sign(vspeed);
	}
	hspeed = 0;
	vspeed = 0;
}

if move_input_total == 0 && hspeed == 0 && vspeed == 0{
	anim_state = 0;
}

if vspeed > 0 {
	anim_state = 3;	
}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 104B8C07
/// @DnDArgument : "code" "/// @description Set Sprite$(13_10)$(13_10)switch (anim_state) {$(13_10)	//Idle$(13_10)    case 0:$(13_10)        sprite_index = spr_PlayerIdle;$(13_10)        break;$(13_10)	//Ground Run$(13_10)	case 1:$(13_10)        sprite_index = spr_PlayerRun;$(13_10)        break;$(13_10)	//Air Up$(13_10)	case 2:$(13_10)        sprite_index = spr_PlayerAirUp;$(13_10)        break;$(13_10)	//Air Down$(13_10)	case 3:$(13_10)        sprite_index = spr_PlayerAirDown;$(13_10)        break;$(13_10)    default:$(13_10)        sprite_index = spr_PlayerIdle;$(13_10)        break;$(13_10)}"
/// @description Set Sprite

switch (anim_state) {
	//Idle
    case 0:
        sprite_index = spr_PlayerIdle;
        break;
	//Ground Run
	case 1:
        sprite_index = spr_PlayerRun;
        break;
	//Air Up
	case 2:
        sprite_index = spr_PlayerAirUp;
        break;
	//Air Down
	case 3:
        sprite_index = spr_PlayerAirDown;
        break;
    default:
        sprite_index = spr_PlayerIdle;
        break;
}