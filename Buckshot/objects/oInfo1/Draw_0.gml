/// @description Draw info texts
/// What adds this template to the basics?

draw_set_alpha(0.5);
var x1 = camera_get_view_x(view_camera[0]) + 10;
var y1 = camera_get_view_y(view_camera[0]) + 10;
var x2 = x1 + 400;
var y2 = y1 + 80;
draw_rectangle_colour(x1, y1, x2, y2, c_white, c_white, c_white, c_white, false); 
draw_set_alpha(1);
draw_text(x1 + 10, y1 + 10, string_hash_to_newline("MEDIUM PLATFORM GAMEPLAY"));
draw_text(x1 + 10, y1 + 30, string_hash_to_newline("- Better movement flow (accel+friction)"));
draw_text(x1 + 10, y1 + 50, string_hash_to_newline("- Smooth camera pan"));

