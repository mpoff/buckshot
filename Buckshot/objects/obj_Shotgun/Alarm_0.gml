/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 2B062140
/// @DnDArgument : "var" "ammo"
/// @DnDArgument : "op" "1"
/// @DnDArgument : "value" "max_ammo"
if(ammo < max_ammo)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 049A72DA
	/// @DnDParent : 2B062140
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "ammo"
	ammo += 1;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2CE8B01A
	/// @DnDParent : 2B062140
	/// @DnDArgument : "var" "ammo"
	/// @DnDArgument : "op" "1"
	/// @DnDArgument : "value" "max_ammo"
	if(ammo < max_ammo)
	{
		/// @DnDAction : YoYo Games.Instances.Set_Alarm
		/// @DnDVersion : 1
		/// @DnDHash : 00EF7D0C
		/// @DnDParent : 2CE8B01A
		/// @DnDArgument : "steps" "60"
		alarm_set(0, 60);
	}
}