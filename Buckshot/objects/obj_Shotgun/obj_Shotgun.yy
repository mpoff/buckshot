{
    "id": "908364f3-f961-47db-9f85-44d9293db54b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Shotgun",
    "eventList": [
        {
            "id": "3450c43b-7c71-4fc7-b013-171b977dc4df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "908364f3-f961-47db-9f85-44d9293db54b"
        },
        {
            "id": "0f08affd-09c2-4f65-be77-2b4442aba242",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "908364f3-f961-47db-9f85-44d9293db54b"
        },
        {
            "id": "2b0ce36a-8872-4e82-b94b-96c864f447aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "908364f3-f961-47db-9f85-44d9293db54b"
        },
        {
            "id": "11f9acb3-77ae-4b89-b136-bb4594080fa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "908364f3-f961-47db-9f85-44d9293db54b"
        },
        {
            "id": "c58c9f4b-303d-4bad-8558-e987de3cf3ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "908364f3-f961-47db-9f85-44d9293db54b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d21042d-3a84-40cc-93ce-72819f775dd4",
    "visible": true
}