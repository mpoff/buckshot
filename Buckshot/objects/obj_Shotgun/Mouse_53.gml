/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3EBCDA36
/// @DnDArgument : "var" "ammo"
/// @DnDArgument : "op" "2"
if(ammo > 0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 477836A5
	/// @DnDParent : 3EBCDA36
	/// @DnDArgument : "expr" "-1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "ammo"
	ammo += -1;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 7C57852E
	/// @DnDParent : 3EBCDA36
	/// @DnDArgument : "steps" "60"
	alarm_set(0, 60);

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 73596809
	/// @DnDParent : 3EBCDA36
	/// @DnDArgument : "soundid" "snd_Shoot"
	/// @DnDSaveInfo : "soundid" "a617b7da-b833-499e-9810-bcc44982b777"
	audio_play_sound(snd_Shoot, 0, 0);

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 3DC13FDE
	/// @DnDParent : 3EBCDA36
	/// @DnDArgument : "xpos" "x"
	/// @DnDArgument : "ypos" "y"
	/// @DnDArgument : "objectid" "obj_Bullet"
	/// @DnDArgument : "layer" ""Items""
	/// @DnDSaveInfo : "objectid" "ef187b88-6fba-4401-b966-10b776925c43"
	instance_create_layer(x, y, "Items", obj_Bullet);

	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 6E915F6E
	/// @DnDApplyTo : 90b24b47-f0af-4f15-b026-0fe0f5ca553b
	/// @DnDParent : 3EBCDA36
	with(obj_Player) {
		/// @DnDAction : YoYo Games.Common.Function_Call
		/// @DnDVersion : 1
		/// @DnDHash : 32938792
		/// @DnDInput : 2
		/// @DnDParent : 6E915F6E
		/// @DnDArgument : "function" "motion_add"
		/// @DnDArgument : "arg" "point_direction(x, y, mouse_x, mouse_y)-180"
		/// @DnDArgument : "arg_1" "10"
		motion_add(point_direction(x, y, mouse_x, mouse_y)-180, 10);
	}
}