/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 34A2BDBF
/// @DnDArgument : "x" "obj_Player.x-32"
/// @DnDArgument : "y" "obj_Player.y-60"
/// @DnDArgument : "caption" ""Ammo: ""
/// @DnDArgument : "var" "ammo"
draw_text(obj_Player.x-32, obj_Player.y-60, string("Ammo: ") + string(ammo));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 2197BCBB
/// @DnDArgument : "x" "obj_Player.x+32"
/// @DnDArgument : "y" "obj_Player.y-60"
/// @DnDArgument : "caption" ""/ ""
/// @DnDArgument : "var" "max_ammo"
draw_text(obj_Player.x+32, obj_Player.y-60, string("/ ") + string(max_ammo));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 74C8AF73
/// @DnDArgument : "x" "obj_Player.x-32"
/// @DnDArgument : "y" "obj_Player.y-80"
/// @DnDArgument : "caption" ""Reload: ""
/// @DnDArgument : "var" "alarm_get(0)"
draw_text(obj_Player.x-32, obj_Player.y-80, string("Reload: ") + string(alarm_get(0)));